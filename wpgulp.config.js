/**
 * WPGulp Configuration File
 *
 * 1. Edit the variables as per your project requirements.
 * 2. In paths you can add <<glob or array of globs>>.
 *
 * @package WPGulp
 */

module.exports = {

	// Project options.
	projectURL: 'pyramidschemebar.local', // Local project URL of your already running WordPress site. Could be something like wpgulp.local or localhost:3000 depending upon your local WordPress setup.
	productURL: './', // Theme/Plugin URL. Leave it like it is, since our gulpfile.js lives in the root folder.
	browserAutoOpen: true,
	injectChanges: true,

	// Style options.
	styleSRC: './assets/styles/style.scss', // Path to main .scss file.
	styleDestination: './', // Path to place the compiled CSS file. Default set to root folder.
	outputStyle: 'compact', // Available options → 'compact' or 'compressed' or 'nested' or 'expanded'
	errLogToConsole: true,
	precision: 10,

	// JS Vendor options.
	jsVendorSRC: './assets/scripts/vendor/*.js', // Path to JS vendor folder.
	jsVendorDestination: './assets/scripts/', // Path to place the compiled JS vendors file.
	jsVendorFile: 'vendor', // Compiled JS vendors file name. Default set to vendors i.e. vendors.js.

	// JS Custom options.
	jsCustomSRC: './assets/scripts/src/*.js', // Path to JS custom scripts folder.
	jsCustomDestination: './assets/scripts/', // Path to place the compiled JS custom scripts file.
	jsCustomFile: 'site', // Compiled JS custom file name. Default set to custom i.e. custom.js.

	// Images options.
	imgSRC: './assets/images/raw/*', // Source folder of images which should be optimized and watched. You can also specify types e.g. raw/**.{png,jpg,gif} in the glob.
	imgDST: './assets/images/', // Destination folder of optimized images. Must be different from the imagesSRC folder.

	// Watch files paths.
	watchStyles: './assets/styles/**/*.scss', // Path to all *.scss files inside css folder and inside them.
	watchJsVendor: './assets/scripts/vendor/*.js', // Path to all vendor JS files.
	watchJsCustom: './assets/scripts/src/*.js', // Path to all custom JS files.
	watchPhp: [ './**/*.php', './**/*.twig' ], // Path to all PHP files.

	// Translation options.
	textDomain: 'onemohrtime', // Your textdomain here.
	translationFile: 'onemohrtime.pot', // Name of the translation file.
	translationDestination: './languages', // Where to save the translation files.
	packageName: 'OneMohrTime', // Package name.
	lastTranslator: '', // Last translator Email ID.
	team: 'Derek Mohr <derek@onemohrti.me>', // Team's Email ID.
	bugReport: 'https://AhmadAwais.com/contact/', // Where can users report bugs.

	// Browsers you care about for autoprefixing. Browserlist https://github.com/ai/browserslist
	// The following list is set as per WordPress requirements. Though, Feel free to change.
	BROWSERS_LIST: [
		'last 20 versions',
		'> 5%',
		'ie >= 9'
	]
};
