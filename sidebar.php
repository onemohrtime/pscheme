<?php
/**
 * The Template for the sidebar containing the main widget area
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

$post    = new Timber\Post();
$post_id = $post->ID;

$context['post'] = $post;
$context['option'] = get_fields('option');
$context['special'] = get_field_object('field_5be25f25af4ab');

$todays_date  = current_time( 'D. m / d', 0 );
$todays_month = current_time( 'M', 0 );
$todays_day   = current_time( 'd', 0 );
$todays_year  = current_time( 'Y', 0 );
$day_of_week  = current_time( 'l', 0 );

$context['todays_date']  = $todays_date;
$context['todays_month'] = $todays_month;
$context['todays_day']   = $todays_day;
$context['todays_year']  = $todays_year;
$context['day_of_week']  = $day_of_week;

$context['global_sidebar'] = Timber::get_widgets( 'sidebar1' );
//$context['sidebar2']     = Timber::get_widgets( 'sidebar2' );

$todays_eventStart = current_time('Y-m-d H:i:s');
$tomorrow          = new DateTime('tomorrow');
$tomorrow          = $tomorrow->format('Y-m-d H:i:s');
$todays_eventEnd   = $tomorrow;
$todays_event      = tribe_get_events([
	'post_type'      => 'tribe_events',
	'order'          => 'ASC',
	'eventDisplay'   => 'custom',
//	'start_date'     => date( 'Y-m-d H', strtotime( '-12 hours' ) ),
//	'end_date'       => date( 'Y-m-d H', strtotime( '+12 hours' ) ),
	'start_date'     => $todays_eventStart,
	'end_date'       => $todays_eventEnd,
	'posts_per_page' => 3,
]);
$context['back_room'] = Timber::get_posts( $todays_event );

Timber::render( array( 'partial/sidebar.twig' ), $context );
