<?php

$context = Timber::get_context();

$post            = new Timber\Post();
$post_id         = $post->ID;
$context['post'] = $post;

$startDate = current_time('Y-m-d H');

$features = tribe_get_events([
	'post_type'      => 'tribe_events',
	'order'          => 'ASC',
	'eventDisplay'   => 'custom',
	'start_date'     => $startDate,
	'posts_per_page' => 4,
	'featured'       => true
]);
$context['featured'] = Timber::get_posts( $features );

$args = tribe_get_events([
	'post_type'      => 'tribe_events',
	'order'          => 'ASC',
	'eventDisplay'   => 'custom',
	'start_date'     => $startDate,
	'posts_per_page' => 25,
]);
$context['posts'] = Timber::get_posts( $args );

$context['cost'] = tribe_get_cost($post_id, true);

Timber::render( 'front-page.twig', $context );
