# The Pyramid Scheme

[![Build Status](https://travis-ci.org/timber/starter-theme.svg)](https://travis-ci.org/timber/starter-theme)

This project was built using [Timber](https://wordpress.org/plugins/timber-library/).

## Build commands

* Run `gulp` to build or `gulp build` to get started
* Run `gulp watch` to develop
* ~Run `gulp build --production` to build for deployment~ **(not set up)**

## Environment

This was originally built using MAMP 5, some local setup may still reflect that. I have set up a PHP Constant to conditionally load **dev** or **production** scripts. Add to `define('WP_ENV', 'dev');` to `wp-content.php` to correctly load **dev** scripts.

## About the Timber starter theme

`static/` is where you can keep your static front-end scripts, styles, or images. In other words, your Sass files, JS files, fonts, and SVGs would live here.

`templates/` contains all of your Twig templates. These pretty much correspond 1 to 1 with the PHP files that respond to the WordPress template hierarchy. At the end of each PHP template, you'll notice a `Timber::render()` function whose first parameter is the Twig file where that data (or `$context`) will be used. Just an FYI.

`bin/` and `tests/` ... basically don't worry about (or remove) these unless you know what they are and want to.
