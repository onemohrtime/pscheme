"use strict";

(function ($) {
  ///////////////
  // FUNCTIONS //
  ///////////////
  // Check if mobile
  function isMobile() {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  }
  /**
   * Get Viewport Dimensions
   * returns object with viewport dimensions to match css in width and height properties
   * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
   */


  var viewportwidth;
  var viewportheight; // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight

  if ('undefined' != typeof window.innerWidth) {
    viewportwidth = window.innerWidth, viewportheight = window.innerHeight;
  } // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
  else if ('undefined' != typeof document.documentElement && 'undefined' != typeof document.documentElement.clientWidth && 0 != document.documentElement.clientWidth) {
      viewportwidth = document.documentElement.clientWidth, viewportheight = document.documentElement.clientHeight;
    } // older versions of IE
    else {
        viewportwidth = document.getElementsByTagName('body')[0].clientWidth, viewportheight = document.getElementsByTagName('body')[0].clientHeight;
      }

  console.log('Your viewport is ' + viewportwidth + ' x ' + viewportheight); // END FUNCTIONS
  //////////////////
  // START JQUERY //
  //////////////////
  // Mobile menu

  $('#menu_toggle, .main-nav__fade').on('click touchstart', function (e) {
    e.preventDefault();
    $('.btn--toggle .menu-grid').toggleClass('open close');
    $('#header').toggleClass('toggled--it');
  }); // Infinate Scroll

  $('#tribe-events').infiniteScroll({
    path: '.tribe-events__pagination > a',
    append: '.event',
    status: '.scroller-status',
    hideNav: '.tribe-events__pagination'
  }); // Toggle menu with `esc`

  if (window.matchMedia('(min-width: 1024px)').matches) {
    $('body').keydown(function (event) {
      if (27 == event.which) {
        $('#menu_toggle').trigger('click');
      }
    });
  } // Add Schema tags for tribe_get_venue_details()


  function add_address_schema() {
    var $address = $('#tribe_address'),
        tribeAddress = $address.find('.tribe-address'),
        street = $address.find('.tribe-street-address'),
        city = $address.find('.tribe-locality'),
        state = $address.find('.tribe-region tribe-events-abbr'),
        zip = $address.find('.tribe-postal-code'),
        country = $address.find('.tribe-country-name');
    street.attr('itemprop', 'streetAddress');
    city.attr('itemprop', 'addressLocality');
    state.attr('itemprop', 'addressRegion');
    zip.attr('itemprop', 'postalCode');
    country.attr('itemprop', 'addressCountry').hide();
  }

  add_address_schema(); // END JQUERY
})(jQuery);
"use strict";
"use strict";

(function ($) {
  /////////////////////////////
  // START UNTAPPD BEER MENU //
  /////////////////////////////
  var untappd = document.getElementById('untappd');

  if (untappd) {
    // const onDeckID        = '66103';
    // const onTapID         = '66191';
    var tapListID = '66193';
    var canBottleListID = '66106';
    var tapListUrl = 'https://business.untappd.com/api/v1/menus/' + tapListID + '?full=true';
    var canBottleListUrl = 'https://business.untappd.com/api/v1/menus/' + canBottleListID + '?full=true';
    var requestHeader = 'c2VhbnZpZGJveEBnbWFpbC5jb206c1hRWTZxNlE5bVE1c2t6cjFMeUs';
    var header = document.createElement('header');
    header.setAttribute('class', 'drink-menu__header');
    untappd.appendChild(header).innerHTML = '<h2 class="drink-menu__header">Beer</h2>';
    $.ajax({
      url: tapListUrl,
      method: 'GET',
      dataType: 'json',
      crossDomain: true,
      beforeSend: function beforeSend(xhr) {
        xhr.setRequestHeader('Authorization', 'Basic ' + requestHeader);
      },
      success: function success(data, txtStatus, xhr) {
        console.log(data.menu.name + ' API Status: ' + xhr.status + ', ' + txtStatus);
        var section = document.createElement('section'),
            header = document.createElement('header'),
            title = document.createElement('h3'),
            list = document.createElement('ul');
        section.setAttribute('class', 'drink-menu__section');
        header.setAttribute('class', 'drink-menu__header');
        title.setAttribute('class', 'drink-menu__section-header');
        list.setAttribute('class', 'drink-menu__list');
        var untappdTapList = data.menu.sections[0].items;
        $.each(untappdTapList, function (i, el) {
          // console.log( el );
          untappd.appendChild(section);
          section.appendChild(title).innerHTML = data.menu.name;
          section.appendChild(list);
          var listItem = document.createElement('li');
          var beerName = el.name,
              beerStyle = el.style,
              beerABV = el.abv,
              beerIBU = el.ibu,
              beerBrewery = el.brewery,
              beerPrice = el.containers[0].price,
              beerSize = el.containers[0].container_size.name;
          list.appendChild(listItem);
          listItem.setAttribute('class', 'beer');
          listItem.innerHTML = '<div class="beer__info"><h4 class="beer__name"><span class="name">' + beerName + '</span><small class="style"> / ' + beerStyle + '</small></h4><div class="beer__details"><span>' + beerABV + '% ABV</span> • <span>' + beerIBU + ' IBU</span> • <span>' + beerBrewery + '</span></div></div><div class="beer__price"><span class="price">$' + beerPrice + '</span><small class="size"> / ' + beerSize + '</small></div>';
        });
      },
      error: function error(data, txtStatus, xhr) {
        console.log('Untappd API Status: ' + xhr.status + ', ' + txtStatus);
        $('#untappd').append('<p>Check out our tap list on <a href="https://untappd.com/v/the-pyramid-scheme/45140" target="_blank">Untappd</a>.</p>');
      }
    });
    $.ajax({
      url: canBottleListUrl,
      method: 'GET',
      dataType: 'json',
      crossDomain: true,
      beforeSend: function beforeSend(xhr) {
        xhr.setRequestHeader('Authorization', 'Basic ' + requestHeader);
      },
      success: function success(data, txtStatus, xhr) {
        console.log(data.menu.name + ' API Status: ' + xhr.status + ', ' + txtStatus);
        var untappdCanBottleList = data.menu.sections;
        $.each(untappdCanBottleList, function (i, el) {
          // console.log( el );
          var section = document.createElement('section'),
              header = document.createElement('header'),
              title = document.createElement('h3'),
              list = document.createElement('ul');
          section.setAttribute('class', 'drink-menu__section');
          header.setAttribute('class', 'drink-menu__header');
          title.setAttribute('class', 'drink-menu__section-header');
          list.setAttribute('class', 'drink-menu__list');
          untappd.appendChild(section);
          section.appendChild(title).innerHTML = el.name;
          section.appendChild(list);

          if ('Rotating Feature' == el.name) {
            var listItem = document.createElement('li');
            var beerName = el.items[0].name,
                beerStyle = el.items[0].style,
                beerABV = el.items[0].abv,
                beerIBU = el.items[0].ibu,
                beerBrewery = el.items[0].brewery,
                beerPrice = el.items[0].containers[0].price,
                beerSize = el.items[0].containers[0].container_size.name;
            list.appendChild(listItem);
            listItem.setAttribute('class', 'beer');
            listItem.innerHTML = '<div class="beer__info"><h4 class="beer__name"><span class="name">' + beerName + '</span><small class="style"> / ' + beerStyle + '</small></h4><div class="beer__details"><span>' + beerABV + '% ABV</span> • <span>' + beerIBU + ' IBU</span> • <span>' + beerBrewery + '</span></div></div><div class="beer__price"><span class="price">$' + beerPrice + '</span><small class="size"> / ' + beerSize + '</small></div>';
          } else {
            var _section = el.items;

            _section.forEach(function (val, i) {
              var listItem = document.createElement('li');
              var beerName = val.name,
                  beerStyle = val.style,
                  beerABV = val.abv,
                  beerIBU = val.ibu,
                  beerBrewery = val.brewery,
                  containerTrue = val.containers[0],
                  beerPrice = val.containers[0] ? '$' + val.containers[0].price : '–',
                  beerSize = val.containers[0] ? '$' + val.containers[0].container_size.name : 'ask our bartender';
              list.appendChild(listItem);
              listItem.setAttribute('class', 'beer');
              listItem.innerHTML = '<div class="beer__info"><h4 class="beer__name"><span class="name">' + beerName + '</span><small class="style"> / ' + beerStyle + '</small></h4><div class="beer__details"><span>' + beerABV + '% ABV</span> • <span>' + beerIBU + ' IBU</span> • <span>' + beerBrewery + '</span></div></div><div class="beer__price"><span class="price">' + beerPrice + '</span><small class="size"> / ' + beerSize + '</small></div>';
            });
          }
        });
      },
      error: function error(data, txtStatus, xhr) {
        console.log('Untappd API Status: ' + xhr.status + ', ' + txtStatus);
        $('#untappd').append('<p>Check out our can & bottle list on <a href="https://untappd.com/v/the-pyramid-scheme/45140" target="_blank">Untappd</a>.</p>');
      }
    });
  } // END UNTAPPD BEER MENU

})(jQuery);
//var WebFontConfig = {
//	google : {
//		families : ['Poppins:400,700']
//	},
//	timeout : 2000
//};
//(function(d) {
//	var wf = d.createElement('script'), s = d.scripts[0];
//	wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
//	wf.async = true;
//	s.parentNode.insertBefore(wf, s);
//})(document);
"use strict";