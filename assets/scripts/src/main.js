( function( $ ) {

	///////////////
	// FUNCTIONS //
	///////////////

	// Check if mobile
	function isMobile() {
		return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test( navigator.userAgent );
	}

	/**
	 * Get Viewport Dimensions
	 * returns object with viewport dimensions to match css in width and height properties
	 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
	 */
	var viewportwidth;
	var viewportheight;

	// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
	if ( 'undefined' != typeof window.innerWidth ) {
		viewportwidth  = window.innerWidth,
		viewportheight = window.innerHeight;
	}

	// IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
	else if ( 'undefined' != typeof document.documentElement && 'undefined' != typeof document.documentElement.clientWidth && 0 != document.documentElement.clientWidth ) {
		viewportwidth  = document.documentElement.clientWidth,
		viewportheight = document.documentElement.clientHeight;
	}

	// older versions of IE
	else {
		viewportwidth  = document.getElementsByTagName( 'body' )[0].clientWidth,
		viewportheight = document.getElementsByTagName( 'body' )[0].clientHeight;
	}
	console.log( 'Your viewport is ' + viewportwidth + ' x ' + viewportheight );

	// END FUNCTIONS

	//////////////////
	// START JQUERY //
	//////////////////

	// Mobile menu
	$( '#menu_toggle, .main-nav__fade' ).on( 'click touchstart', function( e ) {
		e.preventDefault();
		$( '.btn--toggle .menu-grid' ).toggleClass( 'open close' );
		$( '#header' ).toggleClass( 'toggled--it' );
	});

	// Infinate Scroll
	$( '#tribe-events' ).infiniteScroll({
		path: '.tribe-events__pagination > a',
		append: '.event',
		status: '.scroller-status',
		hideNav: '.tribe-events__pagination'
	});

	// Toggle menu with `esc`
	if ( window.matchMedia( '(min-width: 1024px)' ).matches ) {
		$( 'body' ).keydown( function( event ) {
			if ( 27 == event.which ) {
				$( '#menu_toggle' ).trigger( 'click' );
			}
		});
	}

	// Add Schema tags for tribe_get_venue_details()
	function add_address_schema() {
		var $address     = $( '#tribe_address' ),
			tribeAddress = $address.find( '.tribe-address' ),
			street       = $address.find( '.tribe-street-address' ),
			city         = $address.find( '.tribe-locality' ),
			state        = $address.find( '.tribe-region tribe-events-abbr' ),
			zip          = $address.find( '.tribe-postal-code' ),
			country      = $address.find( '.tribe-country-name' );

		street.attr( 'itemprop', 'streetAddress' );
		city.attr( 'itemprop', 'addressLocality' );
		state.attr( 'itemprop', 'addressRegion' );
		zip.attr( 'itemprop', 'postalCode' );
		country.attr( 'itemprop', 'addressCountry' ).hide();
	}
	add_address_schema();

	// END JQUERY

}( jQuery ) );
