( function( $ ) {

	/////////////////////////////
	// START UNTAPPD BEER MENU //
	/////////////////////////////

	const untappd  = document.getElementById( 'untappd' );

	if ( untappd ) {

		// const onDeckID        = '66103';
		// const onTapID         = '66191';
		const tapListID       = '66193';
		const canBottleListID = '66106';

		const tapListUrl       = 'https://business.untappd.com/api/v1/menus/' + tapListID + '?full=true';
		const canBottleListUrl = 'https://business.untappd.com/api/v1/menus/' + canBottleListID + '?full=true';

		const requestHeader = 'c2VhbnZpZGJveEBnbWFpbC5jb206c1hRWTZxNlE5bVE1c2t6cjFMeUs';

		let header = document.createElement( 'header' );

		header.setAttribute( 'class', 'drink-menu__header' );
		untappd.appendChild( header ).innerHTML = '<h2 class="drink-menu__header">Beer</h2>';

		$.ajax({
			url: tapListUrl,
			method: 'GET',
			dataType: 'json',
			crossDomain: true,
			beforeSend: function( xhr ) {
				xhr.setRequestHeader( 'Authorization', 'Basic ' + requestHeader );
			},
			success: function( data, txtStatus, xhr ) {
				console.log( data.menu.name + ' API Status: ' + xhr.status + ', ' + txtStatus );

				let section = document.createElement( 'section' ),
					header  = document.createElement( 'header' ),
					title   = document.createElement( 'h3' ),
					list    = document.createElement( 'ul' );

				section.setAttribute( 'class', 'drink-menu__section' );
				header.setAttribute( 'class', 'drink-menu__header' );
				title.setAttribute( 'class', 'drink-menu__section-header' );
				list.setAttribute( 'class', 'drink-menu__list' );

				let untappdTapList = data.menu.sections[0].items;

				$.each( untappdTapList, function( i, el ) {

					// console.log( el );
					untappd.appendChild( section );
					section.appendChild( title ).innerHTML = data.menu.name;
					section.appendChild( list );

					let listItem    = document.createElement( 'li' );
					let beerName    = el.name,
						beerStyle   = el.style,
						beerABV     = el.abv,
						beerIBU     = el.ibu,
						beerBrewery = el.brewery,
						beerPrice   = el.containers[0].price,
						beerSize    = el.containers[0].container_size.name;

					list.appendChild( listItem );
					listItem.setAttribute( 'class', 'beer' );
					listItem.innerHTML = '<div class="beer__info"><h4 class="beer__name"><span class="name">' + beerName + '</span><small class="style"> / ' + beerStyle + '</small></h4><div class="beer__details"><span>' + beerABV + '% ABV</span> • <span>' + beerIBU + ' IBU</span> • <span>' + beerBrewery + '</span></div></div><div class="beer__price"><span class="price">$' + beerPrice + '</span><small class="size"> / ' + beerSize + '</small></div>';
				});

			},
			error: function( data, txtStatus, xhr ) {
				console.log( 'Untappd API Status: ' + xhr.status + ', ' + txtStatus );
				$( '#untappd' ).append( '<p>Check out our tap list on <a href="https://untappd.com/v/the-pyramid-scheme/45140" target="_blank">Untappd</a>.</p>' );
			}
		});

		$.ajax({
			url: canBottleListUrl,
			method: 'GET',
			dataType: 'json',
			crossDomain: true,
			beforeSend: function( xhr ) {
				xhr.setRequestHeader( 'Authorization', 'Basic ' + requestHeader );
			},
			success: function( data, txtStatus, xhr ) {
				console.log( data.menu.name + ' API Status: ' + xhr.status + ', ' + txtStatus );

				let untappdCanBottleList = data.menu.sections;

				$.each( untappdCanBottleList, function( i, el ) {

					// console.log( el );
					let section = document.createElement( 'section' ),
						header  = document.createElement( 'header' ),
						title   = document.createElement( 'h3' ),
						list    = document.createElement( 'ul' );

					section.setAttribute( 'class', 'drink-menu__section' );
					header.setAttribute( 'class', 'drink-menu__header' );
					title.setAttribute( 'class', 'drink-menu__section-header' );
					list.setAttribute( 'class', 'drink-menu__list' );

					untappd.appendChild( section );
					section.appendChild( title ).innerHTML = el.name;
					section.appendChild( list );

					if ( 'Rotating Feature' == el.name ) {
						let listItem    = document.createElement( 'li' );
						let beerName    = el.items[0].name,
							beerStyle   = el.items[0].style,
							beerABV     = el.items[0].abv,
							beerIBU     = el.items[0].ibu,
							beerBrewery = el.items[0].brewery,
							beerPrice   = el.items[0].containers[0].price,
							beerSize    = el.items[0].containers[0].container_size.name;
						list.appendChild( listItem );
						listItem.setAttribute( 'class', 'beer' );
						listItem.innerHTML = '<div class="beer__info"><h4 class="beer__name"><span class="name">' + beerName + '</span><small class="style"> / ' + beerStyle + '</small></h4><div class="beer__details"><span>' + beerABV + '% ABV</span> • <span>' + beerIBU + ' IBU</span> • <span>' + beerBrewery + '</span></div></div><div class="beer__price"><span class="price">$' + beerPrice + '</span><small class="size"> / ' + beerSize + '</small></div>';
					} else {
						let section   = el.items;
						section.forEach( function( val, i ) {
							let listItem    = document.createElement( 'li' );
							let beerName    = val.name,
								beerStyle   = val.style,
								beerABV     = val.abv,
								beerIBU     = val.ibu,
								beerBrewery = val.brewery,
								containerTrue = val.containers[0],
								beerPrice   = val.containers[0] ? '$' + val.containers[0].price : '–',
								beerSize   = val.containers[0] ? '$' + val.containers[0].container_size.name : 'ask our bartender';

							list.appendChild( listItem );
							listItem.setAttribute( 'class', 'beer' );
							listItem.innerHTML = '<div class="beer__info"><h4 class="beer__name"><span class="name">' + beerName + '</span><small class="style"> / ' + beerStyle + '</small></h4><div class="beer__details"><span>' + beerABV + '% ABV</span> • <span>' + beerIBU + ' IBU</span> • <span>' + beerBrewery + '</span></div></div><div class="beer__price"><span class="price">' + beerPrice + '</span><small class="size"> / ' + beerSize + '</small></div>';
						});
					}
				});

			},
			error: function( data, txtStatus, xhr ) {
				console.log( 'Untappd API Status: ' + xhr.status + ', ' + txtStatus );
				$( '#untappd' ).append( '<p>Check out our can & bottle list on <a href="https://untappd.com/v/the-pyramid-scheme/45140" target="_blank">Untappd</a>.</p>' );
			}
		});
	}

	// END UNTAPPD BEER MENU

}( jQuery ) );
