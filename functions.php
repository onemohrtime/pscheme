<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_theme_support( 'custom-logo' );
		add_theme_support( 'automatic-feed-links' );
		
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'do_faq' ) );
		add_action( 'widgets_init', array( $this, 'tyrannosaurus_register_sidebars' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'loadScripts' ) );
		
		add_shortcode( 'faq', function($attributes = array(), $content = null) {
			// Custom FAQ Shortcode
			return Timber::compile('partial/faq.twig', array(
				'question' => $attributes['question'],
				'answer'   => $content
			) );
		} );
		
		register_nav_menus(
			array(
				'main-nav' => __( 'The Main Menu', 'tyrannosaurus' ),   // main nav in header
				'footer-links' => __( 'Footer Links', 'tyrannosaurus' ) // secondary nav in footer
			)
		);
		
		if( function_exists('acf_add_local_field_group') ):
			include_once('acf.php');
		endif;
		
		if( function_exists('acf_add_options_page') ) {
			include_once('acf-options.php');
		}
		
		parent::__construct();
	}

	function register_post_types() {
		/**
		 * Post Type: Photo Galleries
		 */
		$labels = array(
			"name"               => __( "Photo Galleries", "tyrannosaurus" ),
			"singular_name"      => __( "Photo Gallery", "tyrannosaurus" ),
			"menu_name"          => __( "Past Events", "tyrannosaurus" ),
			"all_items"          => __( "All Galleries", "tyrannosaurus" ),
			"add_new"            => __( "Add Gallery", "tyrannosaurus" ),
			"add_new_item"       => __( "Add New Gallery", "tyrannosaurus" ),
			"edit_item"          => __( "Edit Gallery", "tyrannosaurus" ),
			"new_item"           => __( "New Gallery", "tyrannosaurus" ),
			"view_item"          => __( "View Gallery", "tyrannosaurus" ),
			"view_items"         => __( "View Galleries", "tyrannosaurus" ),
			"search_items"       => __( "Search Gallery", "tyrannosaurus" ),
			"not_found"          => __( "No Gallery found", "tyrannosaurus" ),
			"not_found_in_trash" => __( "No Galleries found in Trash", "tyrannosaurus" ),
		);
		$args = array(
			"label"                  => __( "Photo Galleries", "tyrannosaurus" ),
			"labels"                 => $labels,
			"description"            => "Upload photos if there was an event photographer",
			"public"                 => true,
			"publicly_queryable"     => true,
			"show_ui"                => true,
			"delete_with_user"       => false,
			"show_in_rest"           => false,
			"rest_base"              => "",
			"rest_controller_class"  => "WP_REST_Posts_Controller",
			"has_archive"            => false,
			"show_in_menu"           => true,
			"show_in_nav_menus"      => true,
			"exclude_from_search"    => false,
			"capability_type"        => "post",
			"map_meta_cap"           => true,
			"hierarchical"           => false,
			"rewrite"                => array( "slug" => "photos", "with_front" => true ),
			"query_var"              => true,
			"menu_position"          => 20,
			"menu_icon"              => "dashicons-images-alt2",
			"supports"               => array( "title", "editor", "thumbnail", "excerpt", "comments" ),
			"taxonomies"             => array( "post_tag", "tribe_events_cat" ),
		);
		register_post_type( "photos", $args );
		/**
		 * Post Type: Drink Menus
		 */
		$labels = array(
			"name" => __( "Drink Menus", "tyrannosaurus" ),
			"singular_name" => __( "Drink Menu", "tyrannosaurus" ),
			"menu_name" => __( "Drink Menus", "tyrannosaurus" ),
			"all_items" => __( "All Drink Menus", "tyrannosaurus" ),
			"add_new" => __( "Add New Menu", "tyrannosaurus" ),
			"add_new_item" => __( "Add New Menu", "tyrannosaurus" ),
			"edit_item" => __( "Edit Menu", "tyrannosaurus" ),
			"new_item" => __( "New Menu", "tyrannosaurus" ),
			"view_item" => __( "View Menu", "tyrannosaurus" ),
			"view_items" => __( "View Menus", "tyrannosaurus" ),
			"search_items" => __( "Search Menus", "tyrannosaurus" ),
			"not_found" => __( "No menus found", "tyrannosaurus" ),
			"not_found_in_trash" => __( "No menus found in trash", "tyrannosaurus" ),
			"parent_item_colon" => __( "Parent Menu:", "tyrannosaurus" ),
			"filter_items_list" => __( "Filter menu list", "tyrannosaurus" ),
			"items_list_navigation" => __( "Menu list navigation", "tyrannosaurus" ),
			"items_list" => __( "Drink menu list", "tyrannosaurus" ),
			"attributes" => __( "Drink Menu Attributes", "tyrannosaurus" ),
			"name_admin_bar" => __( "Drink Menu", "tyrannosaurus" ),
			"parent_item_colon" => __( "Parent Menu:", "tyrannosaurus" ),
		);
		$args = array(
			"label" => __( "Drink Menus", "tyrannosaurus" ),
			"labels" => $labels,
			"description" => "Beer menus are imported from <a href=\"https://untappd.com/v/the-pyramid-scheme/45140\" target=\"_blank\">Untappd</a>. The rest of the drink menus are updated here.",
			"public" => true,
			"publicly_queryable" => true,
			"show_ui" => true,
			"delete_with_user" => false,
			"show_in_rest" => true,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => array( "slug" => "drink_menu", "with_front" => true ),
			"query_var" => true,
			"menu_icon" => "dashicons-clipboard",
			"supports" => array( "title" ),
		);
		register_post_type( "drink_menu", $args );
	}
	
	function add_to_context( $context ) {
		$context['menu']    = new TimberMenu();
		$context['site']    = $this;
		$context['context'] = $context;
		
		$custom_logo_id = get_theme_mod('custom_logo');
		$logo           = wp_get_attachment_image_src($custom_logo_id , 'full');
		
		$frontpage_id = get_option('page_on_front');
		$mobile_logo  = get_the_post_thumbnail_url($frontpage_id);
		
		$context['logo']        = $logo;
		$context['mobile_logo'] = $mobile_logo;
		
		$eventsStart = current_time('Y-m-d H');
		$events = tribe_get_events([
			'post_type'      => 'tribe_events',
			'eventDisplay'   => 'custom',
			'start_date'     => $eventsStart,
			'posts_per_page' => -1,
		]);
		$context['events'] = Timber::get_posts( $events );
		
		$context['is_front_page']  = is_front_page();
		$context['sidebar']        = Timber::get_sidebar('sidebar.php');
		$context['option']         = get_fields('option');
		$context['daily_specials'] = get_field('daily_specials');
		
		return $context;
	}
	
//	function myfoo( $text ) {
//		$text .= ' bar!';
//		return $text;
//	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		//$twig->addExtension( new Twig_Extension_StringLoader() );
		//$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}
	
	function loadScripts() {
		
		// Register scripts
		if (defined('WP_ENV')) {
			wp_register_style( 'tyrannosaurus', get_stylesheet_directory_uri() . '/style.css', array(), null, 'all' );
			wp_register_script( 'vendor', get_template_directory_uri() . '/assets/scripts/vendor.js', array('jquery'), null, true );
			wp_register_script( 'tyrannosaurus', get_template_directory_uri() . '/assets/scripts/site.js', array('jquery', 'vendor'), null, true );
		} else {
			wp_register_style( 'tyrannosaurus', get_stylesheet_directory_uri() . '/style.min.css', array(), '2.1.1', 'all' );
			wp_register_script( 'vendor', get_template_directory_uri() . '/assets/scripts/vendor.min.js', array('jquery'), '2.1.1', true );
			wp_register_script( 'tyrannosaurus', get_template_directory_uri() . '/assets/scripts/site.min.js', array('jquery', 'vendor'), '2.1.1', true );
		}
		
		// Enqueue scripts
		wp_enqueue_style( 'tyrannosaurus' );
		wp_enqueue_script( 'vendor' );
		wp_enqueue_script( 'tyrannosaurus' );
		
		// add inline jQuery fallback
//		wp_add_inline_script ('jquery', 'if (!window.jQuery) { document.write(\'<script src="' + get_template_directory_uri() + '/assets/scripts/jquery-3.2.1.min.js"><\/script>\'); }');
	}
	
	function tyrannosaurus_register_sidebars() {
		register_sidebar(array(
			'id'            => 'sidebar1',
			'name'          => __( 'Global Sidebar', 'tyrannosaurus' ),
			'description'   => __( 'The first (primary) sidebar.', 'tyrannosaurus' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4 class="widgettitle">',
			'after_title'   => '</h4>',
		));
	}
	
	// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
	function tyrannosaurus_filter_ptags_on_images($content){
		return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}

	// This removes the annoying […] to a Read More link
	function tyrannosaurus_excerpt_more($more) {
		global $post;
		// edit here if you like
		return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'tyrannosaurus' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', 'tyrannosaurus' ) .'</a>';
	}
	
}

new StarterSite();
